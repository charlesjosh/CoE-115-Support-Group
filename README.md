# CoE 115 | Support Group Repository
## Made specifically for the second semester of AY 2016-2017 of EEEI, UP Diliman

> _papasa tayo mga beshie <3_

## Introduction
---
Here you'll see all resources used (usually by Charles) and published to the group. This allows us to give a better hosting solution for the code snippets and documents posted online.

## What's inside
---
* `lab/` - contains the specs for each lab, as well as the board schematic for easy access.
* `resources/` - contains the syllabi, datasheet, and programmer's reference manual. This will be updated periodically.
* `templates/` - contains code templates for C and Assembly, as well as codes for Blink and Pushbutton.
* `tutorials/` - contains the original files used in the photo albums posted in the Facebook group.
* [Wiki](https://gitlab.com/charlesjosh/CoE-115-Support-Group/wikis/home) - further tips, resources, and information contributed by fellow 115 takers (as of now, mainly Carl).

## General tips
---
* Use the provided templates (at `templates/`) to start coding.
* Make consistent backups of your code! You'll never know when this is necessary.
* All code assumes an **instruction frequency** of **2 MHz**.
* MPLAB X IDE has a built-in **History** function that saves previous versions of your code. This might help in the future.
* Read the other tips from fellow CoE 115 takers at our Facebook group.
* Have fun!

## To the Instructors
---
> Hi there.
>
> I'm Charles. I provide this repository, along with other takers, for fellow takers. The goal of this is to: 
> 1. put importance on critical parts of the code and the programming process that might have glossed over during the lecture and laboratory, or those forgotten by students,
> 2. provide (legal, non-cheatsy) templates that work with both the `PIC24FJ64GB002` and `PIC24FJ64GA002`, 
> 3. have a central repository for resources geared towards helping students in the laboratory,
> 4. provide a jumpstart for students for their code.
>
> No full code implementations or answers for the previous labs are included. I do not condone cheating as much as you guys do, and these only serve as templates for further work done by the students themselves. If there are any comments or suggestions, or that there are resources in this repository that should be removed, please do notify me and I will comply accordingly.
>
> Thanks!
>
> Charles
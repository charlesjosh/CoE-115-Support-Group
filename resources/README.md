# Resources


### What's inside
* `**-syllabus-**.pdf` - lec/lab syllabus
* `**.pdf` - important resources related to the PIC

### Protip
You must learn how to keep these three resources handy at all times, as well how to smartly search through these references.

* Use the ***Datasheet*** for device-specific information. You can potentially survive with this alone, but the datasheet periodically references the two resources below for further discussion.
* Use the ***Programmer's Reference Manual*** for an in-depth explanation of the instructions and their corresponding syntax. Examples are included as well for verification.
* The ***PIC24F Family Reference Manual*** is the main Bible of the PIC24F family. I (painstakingly) downloaded, merged, and bookmarked these manuals for your ease of convenience. This includes an in-depth discussion on major features--they (probably) serve as the main reference for our lectures.

### To the Takers
If the uploaded syllabus differs for your section, please give me a copy immediately. Thanks!
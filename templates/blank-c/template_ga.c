/*
 * File:   your file here
 * Author: your name here
 * Use with PIC24FJ64GA002.
 */

#include "xc.h"

#pragma config FWDTEN = OFF
#pragma config JTAGEN = OFF
#pragma config POSCMOD = NONE
#pragma config OSCIOFNC = ON
#pragma config FCKSM = CSDCMD
#pragma config FNOSC = FRCPLL
#pragma config ICS = PGx2

// It's good practice to define your own functions here.
// They serve as reusable code for your future programming ventures.

int main (void) {
    // Enable this when using the PIC24FJ64GA002.
    // This ensures that your code is running in 2 MHz (Fcy).
    CLKDIV = 0x0300;

    // Setup your initial registers here (ie., TRISx, LATx, AD1PCFG, etc.)
    
    // Code proper starts here.

    return 0;
}
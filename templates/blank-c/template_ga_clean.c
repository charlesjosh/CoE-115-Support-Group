/*
 * File:   your file here
 * Author: your name here
 * Use with PIC24FJ64GA002.
 */

#include "xc.h"

#pragma config FWDTEN = OFF
#pragma config JTAGEN = OFF
#pragma config POSCMOD = NONE
#pragma config OSCIOFNC = ON
#pragma config FCKSM = CSDCMD
#pragma config FNOSC = FRCPLL
#pragma config ICS = PGx2

int main (void) {
    CLKDIV = 0x0300;

    return 0;
}
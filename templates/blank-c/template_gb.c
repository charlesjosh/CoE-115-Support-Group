/*
 * File:   your file here
 * Author: your name here
 * Use with PIC24FJ64GB002.
 */

#include "xc.h"

#pragma config FWDTEN = OFF
#pragma config JTAGEN = OFF
#pragma config POSCMOD = NONE
#pragma config OSCIOFNC = ON
#pragma config FCKSM = CSDCMD
#pragma config FNOSC = FRCPLL
#pragma config ICS = PGx2
#pragma config PLL96MHZ = OFF
#pragma config PLLDIV = NODIV
#pragma config SOSCSEL = IO

// It's good practice to define your own functions here.
// They serve as reusable code for your future programming ventures.

int main (void) {
    // Setup your initial registers here (ie., TRISx, LATx, AD1PCFG, etc.)
    
    // Code proper starts here.

    return 0;
}
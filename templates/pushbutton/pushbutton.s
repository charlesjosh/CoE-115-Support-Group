;; *********************
;; pushbutton.s
;;    This turns LED1 (RA0) on when PB (RB0) is pushed down (shorted).
;;    No debounce is implemented in this code.
;;    Connect LED1 and PB jumpers.
;; *********************

.include "p24FJ64GB002.inc"
; .include "p24FJ64GA002.inc"
.global __reset

;; For the PIC24FJ64GB002, enable these config bits.
config __CONFIG1, FWDTEN_OFF & JTAGEN_OFF & ICS_PGx2
config __CONFIG2, POSCMOD_NONE & OSCIOFNC_OFF & FCKSM_CSDCMD & FNOSC_FRCPLL & PLL96MHZ_OFF & PLLDIV_NODIV

;; For the PIC24FJ64GA002, enable these config bits.
; config __CONFIG1, FWDTEN_OFF & JTAGEN_OFF & ICS_PGx2
; config __CONFIG2, POSCMOD_NONE & OSCIOFNC_OFF & FCKSM_CSDCMD & FNOSC_FRCPLL

.bss
 
.text
__reset:
    ;; Initialize the stack pointer register and stack pointer limit register.
    mov #__SP_init, W15
    mov #__SPLIM_init, W0
    mov W0, SPLIM

    ;; For the PIC24FJ64GA002, enable this register write.
    ;; This ensures that your code is running in 2 MHz.
    ; mov #0x0300, W0
    ; mov W0, CLKDIV

    ;; Since RB0 is designated as an Analog Input pin (AN2),
    ;;    it is necessary to tell the microcontroller to use
    ;;    RB0 as a digital pin. 
    ;; The AD1PCFG register (A/D Port Configuration Register)
    ;;    sets a pin to either digital (1), enabling I/O port read,
    ;;    or analog (0), disabling I/O port read, but samples the pin
    ;;    voltage (ADC)
    mov #0xFFFF, W0
    mov W0, AD1PCFG

    ;; Set RA0 (connected to LED 1) to output.
    ;; Setting a TRIS bit to 1 makes a pin an input, 0 an output.
    ;; 0xFFFE is (1111 1111 1111 1110) in binary, and bit 0 (LSB)
    ;;    corresponds to RA0.
    mov #0xFFFE, W0
    mov W0, TRISA

    ;; Set all pins in port B as an input.
    ;; We generally just want RB0 to be an input, but convention
    ;;    states that we set all unused pins to input anyway.
    mov #0xFFFF, W0
    mov W0, TRISB
 
main:
waitdown:
    ;; Sets bit 0 of LATA, writing a high (VDD) to RA0.
    ;; As discussed, this turns off the LED.
    bset LATA, #0

    ;; Reads RB0. If RB0 is clear (PB is pushed down),
    ;;    skip the next line (effectively going to waitup).
    ;; If RB0 is set (PB is not pushed down),
    ;;    execute the next line (loop back to waitdown).
    btsc PORTB, #0
    goto waitdown 
 
waitup:
  ;; Clears bit 0 of LATA, writing a low (GND) to RA0.
  ;; As discussed this turns on the LED.
	bclr LATA, #0

  ;; Reads RB0. If RB0 is set (PB is not pushed down),
  ;;    skip the next line (loop back to main, then waitdown).
  ;; If RB0 is clear (PB is pushed down),
  ;;    execute the next line (loop back to waitup).
	btss PORTB, #0
	goto waitup

	goto main
 
.end
